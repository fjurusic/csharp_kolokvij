﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chsarp_Kolokvij_FilipJurusic {
    interface IPoziv {
        void SpojiKorisnika(Korisnik korisnik);
        void PrikaziKorisnike();
    }
}
