﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chsarp_Kolokvij_FilipJurusic {
    abstract class Poziv {
        protected DateTime vrijemePocetka;
        protected DateTime vrijemeZavrsetka;
        protected string lozinka;
        protected List<Korisnik> korisnici;

        public Poziv(DateTime vrijemePocetka, DateTime vrijemeZavrsetka, string lozinka) {
            VrijemePocetka = vrijemePocetka;
            VrijemeZavrsetka = vrijemeZavrsetka;
            Lozinka = lozinka;
            Korisnici = new List<Korisnik>();
        }

        public abstract void SpojiKorisnika(Korisnik korisnik);

        public virtual void PrikaziKorisnike() {
            foreach(Korisnik k in Korisnici) {
                Console.WriteLine(k);
            }
        }


        public DateTime VrijemePocetka { get; set; }
        public DateTime VrijemeZavrsetka {
            get { return vrijemeZavrsetka; }
            set {
                if (value > vrijemeZavrsetka) {
                    vrijemeZavrsetka = value;
                } else {
                    throw new Exception("Nevaljano vrijeme završetka.");
                }
            }
        }
        public string Lozinka { get; set; }
        public List<Korisnik> Korisnici { get; set; }
    }
}
