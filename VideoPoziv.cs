﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chsarp_Kolokvij_FilipJurusic {
    class VideoPoziv : Poziv, IPoziv {
        public VideoPoziv(DateTime vrijemePocetka, DateTime vrijemeZavrsetka, string lozinka) : base(vrijemePocetka, vrijemeZavrsetka, "123456") {}

        public override void SpojiKorisnika(Korisnik korisnik) {
            if(korisnik.Mikrofon == true && korisnik.Kamera == true) {
                Console.WriteLine("Korisnik spojen");
                Korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema potrebnu opremu");
            }
        }

    }
}
