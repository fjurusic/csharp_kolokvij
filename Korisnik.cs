﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chsarp_Kolokvij_FilipJurusic {
    class Korisnik {
        private string username;
        private string ime;
        private int jedinstveniBroj;
        private bool mikrofon;
        private bool kamera;

        public Korisnik(string username, string ime, int jedinstveniBroj, bool mikrofon, bool kamera) {
            Username = username;
            Ime = ime;
            JedinstveniBroj = jedinstveniBroj;
            Mikrofon = mikrofon;
            Kamera = kamera;
        }

        public override string ToString() {
            return string.Format("{0} / {1} / {2}", JedinstveniBroj, Username, Ime );
        }


        public string Username { get; set; }
        public string Ime { get; set; }
        public int JedinstveniBroj { 
            get {
                return jedinstveniBroj;
            } 
            set {
                if(value > 100000) {
                    jedinstveniBroj = value;
                } else {
                    throw new Exception("Nevaljani jednistveni broj.");
                }
            } 
        }
        public bool Mikrofon { get; set; }
        public bool Kamera { get; set; }
    }
}
