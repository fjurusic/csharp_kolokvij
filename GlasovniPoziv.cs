﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chsarp_Kolokvij_FilipJurusic {
    class GlasovniPoziv : Poziv, IPoziv {

        public GlasovniPoziv(DateTime vrijemePocetka, DateTime vrijemeZavrsetka, string lozinka) : base(vrijemePocetka, vrijemeZavrsetka, "123456") {
            
        }
        public override void SpojiKorisnika(Korisnik korisnik) {
            if (korisnik.Mikrofon == true && Korisnici.Count <= 16) {
                Console.WriteLine("Korisnik spojen");
                Korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema mikrofon.");
            }
        }

        public override void PrikaziKorisnike() {
            foreach (Korisnik k in Korisnici) {
                if(k.Kamera == false) {
                    Console.WriteLine(k);
                }
            }
        }
    }
}
